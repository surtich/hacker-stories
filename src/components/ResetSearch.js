import React from "react";

export const ResetSearch = ({onReset}) => {
    return (
        <div>
            <input type="button" value="reset search" onClick={onReset}/>
        </div>
    );
};
