export default function withLoading(WrappedComponent, loadingMessage = "loading...") {
    return ({ loading, ...props }) => loading ? <p>{loadingMessage}</p> : <WrappedComponent {...props} />;
}