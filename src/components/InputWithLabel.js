import React from "react";

export const InputWithLabel = ({ id, type = "text", onInputChange, value, children, isFocused=false }) => {
    const inputRef = React.useRef();

    React.useEffect(() => {
        if (isFocused && inputRef.current) {        
            inputRef.current.focus();
        }
    }, [isFocused]);

    return (
        <>
            <label htmlFor={id}>{children}</label>
            &nbsp;
            <input id={id} ref={inputRef} type={type} value={value} onChange={onInputChange} />
        </>
    );
};
