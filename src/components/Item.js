import React from "react";
import { StoryContext } from "../context/StoryContext";
import styled, { css } from 'styled-components';
import withLoading from "./hoc/withLoading";

const ItemContainer = styled.div`  
    ${props => props.removed && css`
            text-decoration: line-through;
        `
    }
`;

const ItemContainerWithLoading = withLoading(ItemContainer, "removing...");

export const Item = React.memo(({ item }) => {
    const { title, url, author, num_comments, points, removed, removing } = item;
    const { onRemoveStory } = React.useContext(StoryContext);

    return <ItemContainerWithLoading loading={removing} removed={removed}>
        <span>
            <a href={url}>{title}</a>
        </span>
        <span>{author}</span>
        <span>{num_comments}</span>
        <span>{points}</span>
        <span>
            <button type="button" onClick={() => onRemoveStory(item)}>Dismiss</button>
        </span>
    </ItemContainerWithLoading>
});
