import React from "react";
import { List } from "./List";
import { ResetSearch } from "./ResetSearch";
import { InputWithLabel as SearchInput } from "./InputWithLabel";
import { StoryContext } from "../context/StoryContext";
import useDebounce from "../hooks/useDebounce";
import { storiesReducer, storyActions, initialStoriesState } from "../reducers/storyReducer";
import { initialSearch, getAsyncStories, removeAsyncStory } from "../services/storyServices";
import useSemiPersistentState from "../hooks/useSemiPersistentState";
import withLoading from "./hoc/withLoading";


const App = () => {

  const [searchTerm, setSearchTerm] = useSemiPersistentState("search", initialSearch);
  const debouncedSearch = useDebounce(searchTerm, 200);
  const searchVersionRef = React.useRef(0);

  const [storiesState, dispatchStories] = React.useReducer(storiesReducer, initialStoriesState);

  const loadStories = React.useCallback(async () => {
    const searchVersion = searchVersionRef.current + 1;
    searchVersionRef.current = searchVersion;
    dispatchStories({
      type: storyActions.fecthInit
    });
    try {
      const result = await getAsyncStories(debouncedSearch)
      if (searchVersion === searchVersionRef.current) {
        dispatchStories({
          type: storyActions.fecthSuccess,
          payload: { stories: result.stories }
        });
      }
    } catch (_) {
      dispatchStories({
        type: storyActions.fecthFailure
      });;
    };
  }, [debouncedSearch]);

  React.useEffect(loadStories, [loadStories]);

  const { stories, isLoading, isError } = storiesState;

  const handleRemoveStory = React.useCallback(story => {
    /*dispatchStories({
      type: storyActions.remove,
      payload: { objectID: story.objectID }
    });*/

    dispatchStories({
      type: storyActions.removeInit,
      payload: { objectID: story.objectID }
    })

    removeAsyncStory(story.objectID).then(payload => dispatchStories({
      type: storyActions.removeSuccess,
      payload
    }));


  }, []);

  const contextValue = React.useMemo(() => ({
    onRemoveStory: handleRemoveStory
  }), [handleRemoveStory]);

  const handleSearch = event => {
    setSearchTerm(event.target.value);
  };

  const handleReset = () => {
    setSearchTerm(initialSearch);
  };


  const ListWithLoading = withLoading(List);

  return (<div>
    <h1>My Hacker Stories</h1>
    <form onSubmit={loadStories}>
      <ResetSearch onReset={handleReset} />
      <button disabled={!searchTerm.trim()} type="submit">Reload</button>
      <SearchInput id="search" onInputChange={handleSearch} value={searchTerm} isFocused>
        <strong>Search</strong>
      </SearchInput>
    </form>
    <hr />
    <StoryContext.Provider value={contextValue}>
      {isError && <p>Something went wrong ...</p>}
      <ListWithLoading loading={isLoading} list={stories} />
    </StoryContext.Provider>
  </div>)
};

export default App;
