import React from "react";
import { Item } from "./Item";

export const List = React.memo(({ list }) => list.map(item => <Item key={item.objectID} item={item} />));
