export const initialSearch = "React";
const API_ENDPOINT = 'https://hn.algolia.com/api/v1/search?query=';
export const getAsyncStories = (search = initialSearch) => fetch(API_ENDPOINT + search)
  .then(response => response.json())
  .then(result => ({
    stories: result.hits
  }));

export const removeAsyncStory = objectID => new Promise(function (resolve, _reject) {
  setTimeout(function () {
    resolve({
      objectID
    });
  }, 1000);
})
