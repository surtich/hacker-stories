import React from "react";

export default function useSemiPersistentState(key, initialState = "") {

    const isMounted = React.useRef(false);
    const [value, setValue] = React.useState(localStorage.getItem(key) || initialState);

    React.useEffect(() => {
        if (isMounted.current) {
            localStorage.setItem(key, value);
        }
        isMounted.current = true;
    }, [key, value]);

    return [value, setValue];
};
