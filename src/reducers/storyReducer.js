import testUtils from "react-dom/test-utils";

export const initialStoriesState = {
  stories: [],
  isLoading: false,
  isError: false
}

export const storyActions = {
  fecthInit: "STORIES_FETCH_INIT",
  fecthSuccess: "STORIES_FETCH_SUCCESS",
  fecthFailure: "STORIES_FETCH_FAILURE",
  removeInit: "STORIES_REMOVE_INIT",
  removeSuccess: "REMOVE_STORY_SUCCESS"
};


export const storiesReducer = (storiesState, action) => {
  switch (action.type) {
    case storyActions.fecthInit:
      return {
        ...storiesState,
        isLoading: true,
        isError: false
      };
    case storyActions.fecthSuccess:
      return {
        stories: action.payload.stories.map(story => ({ ...story, removed: false })),
        isLoading: false,
        isError: false
      };
    case storyActions.fecthFailure:
      return {
        ...storiesState,
        isLoading: false,
        isError: true
      };
    case storyActions.removeInit:
      return {
        ...storiesState,
        stories: storiesState.stories.map(story => ({ ...story, removing: story.removing || story.objectID === action.payload.objectID })),
        isLoading: false,
        isError: false
      };
    case storyActions.removeSuccess:
      return {
        ...storiesState,
        isLoading: false,
        stories: storiesState.stories.map(story => ({
          ...story,
          removed: story.removed ||
            story.objectID === action.payload.objectID,
          removing: story.objectID === action.payload.objectID ? false : story.removing
        }))
      };

    default:
      return storiesState;
  }
};
